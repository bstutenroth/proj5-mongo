import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import datetime
import logging
from bson.json_util import dumps

app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.calcdb
d={}

@app.route('/')
def calc():
    _items = db.calcdb.find()
    items = [item for item in _items]

    return render_template('calc.html', items=items)

@app.route('/hi')
def hi():

    return render_template('hi.html')


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

'''
@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404
'''

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    
    km = request.args.get('km', 999, type=float)
    open_time = arrow.now().isoformat
    close_time = arrow.now().isoformat
    client = MongoClient('localhost', 5000)
    db = client.tracks
    collection = db.samples

    post = {"km": km,
         "open": open_time,
         "close": close_time}
    posts = db.posts
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, 200, arrow.now().isoformat)
    close_time = acp_times.close_time(km, 200, arrow.now().isoformat)
    populate_db(["dist", "ot", "ct"], [km, open_time, close_time])
    
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

def populate_db(l1, l2):
    for i in range(len(l2)):
        d[l1[i]] = l2[i]
    return render_template('hi.html',variable=d)
    
#############

app.debug = CONFIG.DEBUG
if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
if app.debug:
    app.logger.setLevel(logging.DEBUG)


